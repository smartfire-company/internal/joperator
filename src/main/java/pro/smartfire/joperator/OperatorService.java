package pro.smartfire.joperator;

import io.fabric8.kubernetes.api.model.apiextensions.CustomResourceDefinition;
import io.fabric8.kubernetes.api.model.apiextensions.CustomResourceDefinitionBuilder;
import io.fabric8.kubernetes.client.*;
import io.fabric8.kubernetes.client.dsl.MixedOperation;
import io.fabric8.kubernetes.client.dsl.Resource;
import io.fabric8.kubernetes.internal.KubernetesDeserializer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.lang.annotation.Annotation;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Operator Service is the class in charge of matching CRD to real life.
 */
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({"WeakerAccess", "unused"})
public class OperatorService {

    private static final String KUBERNETES_EXTENSIONS_API_VERSION = "apiextensions.k8s.io/v1beta1";

    private final ApplicationContext applicationContext;
    private final KubernetesClient kubernetesClient;
    private HashMap<String, CustomResourceDefinition> managedCrd = new HashMap<>();
    private HashMap<String, Class<? extends CustomResource>> managedCrdClasses = new HashMap<>();
    private String namespace;

    @PostConstruct
    private void setup() {
        namespace = kubernetesClient.getNamespace();
        log.info("Operator is running on namespace {}", namespace);

        fetchAllCrd().parallelStream().forEach(crdClass -> {
            final CustomResourceDefinition crd = asCRD(crdClass);
            if (crd == null) {
                log.error("{} is not a valid Kubernetes resource", crdClass.getName());
                return;
            }
            final String name = crd.getMetadata().getName();
            managedCrd.put(name, crd);
            managedCrdClasses.put(name, crdClass);
        });
        HashMap<String, CustomResourceDefinition> runningCrds = new HashMap<>();
        kubernetesClient.customResourceDefinitions().list().getItems().forEach(crd -> runningCrds.put(crd.getMetadata().getName(), crd));

        managedCrd.replaceAll(
                (crdName, customResourceDefinition) ->
                        runningCrds.computeIfAbsent(crdName,
                                ignored -> kubernetesClient.customResourceDefinitions().create(customResourceDefinition)
                        )
        );

        managedCrd.forEach((crdName, crd) ->
                KubernetesDeserializer.registerCustomKind(
                        crd.getSpec().getGroup() + "/" + crd.getSpec().getVersion(),
                        crd.getSpec().getNames().getKind(),
                        managedCrdClasses.get(crdName)));
    }

    @PreDestroy
    private void stop() {
        kubernetesClient.close();
    }

    private CustomResourceDefinition asCRD(Class<? extends CustomResource> clazz) {
        final CustomResourceDefinitionBuilder builder = new CustomResourceDefinitionBuilder();

        builder.withApiVersion(KUBERNETES_EXTENSIONS_API_VERSION);

        final Annotation[] annotationsByType = clazz.getAnnotationsByType(KubernetesCustomResource.class);
        if (annotationsByType.length != 1) {
            return null;
        }
        KubernetesCustomResource crd = (KubernetesCustomResource) annotationsByType[0];
        return builder.withNewMetadata().withName(crd.pluralName() + "." + crd.group()).endMetadata()
                .withNewSpec()
                .withVersion(crd.version())
                .withGroup(crd.group())
                .withNewNames()
                .withKind(crd.name())
                .withShortNames(crd.shortName())
                .withPlural(crd.pluralName())
                .endNames()
                .endSpec().build();
    }

    private List<Class<? extends CustomResource>> fetchAllCrd() {
        final List<Class<? extends CustomResource>> list = new ArrayList<>();

        final Map<String, Object> beans = applicationContext.getBeansWithAnnotation(EnableKubernetesOperators.class);

        beans.values().parallelStream().map(Object::getClass).map(Class::getPackage).map(this::fetchAllCrd).forEach(list::addAll);

        return list;
    }

    @SuppressWarnings("unchecked")
    private List<Class<? extends CustomResource>> fetchAllCrd(Package root) {
        ClassPathScanningCandidateComponentProvider scanner =
                new ClassPathScanningCandidateComponentProvider(false);

        scanner.addIncludeFilter(new AnnotationTypeFilter(KubernetesCustomResource.class));
        scanner.addIncludeFilter(new AssignableTypeFilter(CustomResource.class));

        return scanner.findCandidateComponents(root.getName()).stream().map(beanDefinition -> {
            try {
                return (Class<? extends CustomResource>) Objects.requireNonNull(applicationContext.getClassLoader()).loadClass(beanDefinition.getBeanClassName());
            } catch (ClassNotFoundException e) {
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    public <T extends CustomResource> void watch(Class<T> resourceClass, OperatorListener<T> listener) {
        final CustomResourceDefinition crdSupposed = asCRD(resourceClass);
        if (crdSupposed == null) return;
        final MixedOperation<T, ? extends CustomResourceList<T>, ? extends CustomResourceDoneable<T>, Resource<T, ? extends CustomResourceDoneable<T>>> client = getClientFor(crdSupposed, resourceClass);
        client.inNamespace(namespace).watch(new Watcher<T>() {
            @Override
            public void eventReceived(Action action, T resource) {
                listener.actOn(action, resource);
            }

            @Override
            public void onClose(KubernetesClientException cause) {
                log.error("Kubernetes clauses communication", cause);
            }
        });
    }

    @SuppressWarnings("unchecked")
    public <T extends CustomResource> void delete(T object) {
        final CustomResourceDefinition crdSupposed = asCRD(object.getClass());
        if (crdSupposed == null) return;
        getClientFor(crdSupposed, (Class<T>) object.getClass()).inNamespace(namespace).delete(object);
    }

    @SuppressWarnings("unchecked")
    public <T extends CustomResource> void save(T object) {
        final CustomResourceDefinition crdSupposed = asCRD(object.getClass());
        if (crdSupposed == null) return;
        getClientFor(crdSupposed, (Class<T>) object.getClass()).inNamespace(namespace).createOrReplace(object);
    }

    public <T extends CustomResource> T get(Class<T> object, String name) {
        final CustomResourceDefinition crdSupposed = asCRD(object);
        if (crdSupposed == null) return null;
        return getClientFor(crdSupposed, object).inNamespace(namespace).withName(name).get();
    }

    public <T extends CustomResource> CustomResourceList<T> list(Class<T> object) {
        final CustomResourceDefinition crdSupposed = asCRD(object);
        if (crdSupposed == null) return new CustomResourceList<>();
        return getClientFor(crdSupposed, object).inNamespace(namespace).list();
    }

    @SuppressWarnings("unchecked")
    private <T extends CustomResource> MixedOperation<T, ? extends CustomResourceList<T>, ? extends CustomResourceDoneable<T>, Resource<T, ? extends CustomResourceDoneable<T>>> getClientFor(CustomResourceDefinition crd, Class<T> crdClass) {
        final String crdName = crd.getMetadata().getName();
        crd = managedCrd.get(crdName);
        final KubernetesCustomResource resourceClassAnnotation = crdClass.getAnnotation(KubernetesCustomResource.class);
        return (MixedOperation<T, ? extends CustomResourceList<T>, ? extends CustomResourceDoneable<T>, Resource<T, ? extends CustomResourceDoneable<T>>>) kubernetesClient.customResources(crd, crdClass, resourceClassAnnotation.listClass(), resourceClassAnnotation.doneableClass());
    }


}
