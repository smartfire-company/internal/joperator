package pro.smartfire.joperator;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Enable Kubernetes Operators for the current Spring Application.
 * The base package to scan for CRDs is the package of class annoted.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(JoperatorConfiguration.class)
public @interface EnableKubernetesOperators {
    String value() default ".";
}
