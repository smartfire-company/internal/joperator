package pro.smartfire.joperator;

import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class JoperatorConfiguration {

    @Bean
    @ConditionalOnClass(KubernetesClient.class)
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public OperatorService operatorService(ApplicationContext context, KubernetesClient client){
        return new OperatorService(context, client);
    }

}
