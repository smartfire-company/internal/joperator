package pro.smartfire.joperator;

import io.fabric8.kubernetes.client.CustomResourceDoneable;
import io.fabric8.kubernetes.client.CustomResourceList;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface KubernetesCustomResource {
    String version() default "v1";
    String group();
    String name();
    String shortName();
    String pluralName();
    Class<? extends CustomResourceList> listClass() default CustomResourceKind.List.class;
    Class<? extends CustomResourceDoneable> doneableClass() default CustomResourceKind.Doneable.class;
}
