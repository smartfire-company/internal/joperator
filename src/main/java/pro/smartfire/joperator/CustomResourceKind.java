package pro.smartfire.joperator;

import io.fabric8.kubernetes.api.builder.Function;
import io.fabric8.kubernetes.client.CustomResource;
import io.fabric8.kubernetes.client.CustomResourceDoneable;
import io.fabric8.kubernetes.client.CustomResourceList;

public abstract class CustomResourceKind<T> extends CustomResource {

    public CustomResourceKind() {
        final KubernetesCustomResource annotation = getClass().getAnnotation(KubernetesCustomResource.class);
        if(annotation == null) {
            setKind(getClass().getName());
        } else {
            setKind(annotation.name());
        }
    }

    public CustomResourceKind(String kind) {
        super(kind);
    }

    private T spec;

    public T getSpec() {
        return spec;
    }

    public void setSpec(T spec) {
        this.spec = spec;
    }

    static class List<T> extends CustomResourceList<CustomResourceKind<T>> {
    }

    static class Doneable<T>  extends CustomResourceDoneable<CustomResourceKind<T>> {
        public Doneable(CustomResourceKind<T> resource, Function<CustomResourceKind<T>, CustomResourceKind<T>> function) {
            super(resource, function);
        }
    }
}
