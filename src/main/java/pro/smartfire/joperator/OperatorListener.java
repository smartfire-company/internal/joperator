package pro.smartfire.joperator;

import io.fabric8.kubernetes.client.CustomResource;
import io.fabric8.kubernetes.client.Watcher;

public interface OperatorListener<T extends CustomResource> {

    void actOn(Watcher.Action action, T element);

}
