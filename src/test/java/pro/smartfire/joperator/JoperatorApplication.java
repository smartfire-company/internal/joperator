package pro.smartfire.joperator;

import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.server.mock.KubernetesServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableKubernetesOperators
@Configuration
public class JoperatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(JoperatorApplication.class, args);
    }

    public KubernetesServer server = new KubernetesServer(true, true);

    @Bean
    public KubernetesClient getClient(){
        server.before();
        return server.getClient();
    }

}
