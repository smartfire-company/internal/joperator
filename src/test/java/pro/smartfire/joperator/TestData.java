package pro.smartfire.joperator;

import lombok.Data;

@Data
public class TestData {

    private String website;
    private String firstName;
    private String lastName;

}
