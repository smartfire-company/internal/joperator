package pro.smartfire.joperator;

@KubernetesCustomResource(
        group = "test.smartfire.pro",
        version = "v1test",
        name = "TestData", shortName = "testdata", pluralName = "testdatas"
)
class TestDataResource extends CustomResourceKind<TestData> {}
