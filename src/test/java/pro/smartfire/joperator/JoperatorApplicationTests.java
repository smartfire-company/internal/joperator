package pro.smartfire.joperator;

import io.fabric8.kubernetes.client.KubernetesClient;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import io.fabric8.kubernetes.client.server.mock.KubernetesServer;

import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class JoperatorApplicationTests {

    @Autowired
    private OperatorService operatorService;

    @Autowired
    private KubernetesClient client;

    @Test
    public void contextLoads() {
        final TestData data = new TestData();
        final TestDataResource resource = new TestDataResource();
        resource.setSpec(data);
        resource.getMetadata().setAnnotations(Collections.singletonMap("testing", "true"));
        resource.getMetadata().setName("testresource");
        operatorService.save(resource);
        final TestDataResource loadedResource = operatorService.get(TestDataResource.class, "testresource");
        Assert.assertNotNull(loadedResource);
        Assert.assertNotNull(loadedResource.getMetadata().getAnnotations());
        Assert.assertEquals("Metadata failed to be validated", loadedResource.getMetadata().getAnnotations().get("testing"), "true");
    }

}
