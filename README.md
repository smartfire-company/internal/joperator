# JOperator

JOperator is a wrapper around https://github.com/fabric8io/kubernetes-client/ and https://github.com/spring-cloud/spring-cloud-kubernetes to easly support Kubernetes CRD as part of Java

## Usage

```xml
<dependency>
    <groupId>pro.smartfire</groupId>
    <artifactId>joperator</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```

In your Spring Boot application, annotate your Spring Application class with:
`@EnableKubernetesOperators`

Then to enable for exemple a class like `TestData` to be used as a CRD, create the class `TestDataResource`:

```java
@KubernetesCustomResource(
        group = "test.smartfire.pro",
        version = "v1test",
        name = "TestData", shortName = "testdata", pluralName = "testdatas"
)
class TestDataResource extends CustomResourceKind<TestData> {}
```

You can now use the `OperatorService` like:

```java
@Service
@Slf4j
public class MySuperService {

    @Autowired
    private OperatorService operatorService;
    
    public void demo() {
        final TestData data = new TestData();
        final TestDataResource resource = new TestDataResource();
        resource.setSpec(data);
        resource.getMetadata().setAnnotations(Collections.singletonMap("testing", "true"));
        resource.getMetadata().setName("testresource");
        operatorService.save(resource);
        final TestDataResource loadedResource = operatorService.get(TestDataResource.class, "testresource");
        operatorService.list(TestDataResource.class);
        operatorService.watch(TestDataResource.class, (action, element) -> {});
    }

}
```
